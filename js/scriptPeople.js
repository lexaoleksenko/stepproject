
let btnNext = document.getElementById("people-btn-right");
let btnPrev = document.getElementById("people-btn-left");
let txt = document.querySelectorAll('.slider-txt-item');
let bigImg = document.querySelectorAll('.big-img-people-item');
let navImg = document.querySelectorAll('.people-nav-item');

let itemIndex = 1;
let imgIndex = 1;
let txtIndex = 1;

// Обработка клика на кнопку вперёд
btnNext.addEventListener("click", function (){
    showItem(itemIndex += 1);
});

btnNext.addEventListener("click", function (){
    showbigImg(imgIndex += 1);
});

btnNext.addEventListener("click", function (){
    showTxt(txtIndex += 1);
});


// // Обработка клика на кнопку назад
btnPrev.addEventListener("click", function (){
    showItem(itemIndex -= 1);
});

btnPrev.addEventListener("click", function (){
    showbigImg(imgIndex -= 1);
});

btnPrev.addEventListener("click", function (){
    showTxtPrev(txtIndex -= 1);
});



// Показ нужных блоков
    function showItem(n){
        if(n > navImg.length){
            itemIndex = 1
        }
        if(n < 1){
            itemIndex=navImg.length
        }
        for (i = 0; i < navImg.length; i++) {
            navImg[i].classList.remove('activ-people-nav');
            navImg[i].classList.add('inactiv-people-nav');

        }
        navImg[itemIndex - 1].classList.add("activ-people-nav");
        navImg[itemIndex - 1].classList.remove("inactiv-people-nav");
        
    }
    function showbigImg(n){
        if(n > bigImg.length){
            imgIndex = 1
        }
        if(n < 1){
            imgIndex=bigImg.length
        }
        for (i = 0; i < bigImg.length; i++) {
            bigImg[i].classList.remove('activ-people');
        }
        bigImg[imgIndex - 1].classList.add("activ-people");
    }
    function showTxt(n){
        if(n > txt.length){
            txtIndex = 1
        }
        if(n < 1){
            txtIndex=txt.length
        }
        for (i = 0; i < txt.length; i++) {
            txt[i].classList.remove('slider-txt-activ');
            txt[i].classList.remove('slider-txt-item-prev');
            txt[i].classList.add('slider-txt-item');
        }
        txt[txtIndex - 1].classList.add("slider-txt-activ");
    }

    function showTxtPrev(n){
        if(n > txt.length){
            txtIndex = 1
        }
        if(n < 1){
            txtIndex=txt.length
        }
        for (i = 0; i < txt.length; i++) {
            txt[i].classList.remove('slider-txt-activ');
            txt[i].classList.remove('slider-txt-item');
            txt[i].classList.add('slider-txt-item-prev');
        }
        txt[txtIndex - 1].classList.add("slider-txt-activ");
    }


// Показ блоков по клику в нав

navImg[0].addEventListener('click', function(){
    showItem(itemIndex = 1)
    showbigImg(imgIndex = 1)
    if(txtIndex > 1){
    showTxtPrev(txtIndex = 1)
    } else {
        showTxt(txtIndex = 1)
    }
})

navImg[1].addEventListener('click', function(){
    showItem(itemIndex = 2)
    showbigImg(imgIndex = 2)
    if(txtIndex > 2){
        showTxtPrev(txtIndex = 2)
        } else {
            showTxt(txtIndex = 2)
        }
})

navImg[2].addEventListener('click', function(){
    showItem(itemIndex = 3)
    showbigImg(imgIndex = 3)
    if(txtIndex > 3){
        showTxtPrev(txtIndex = 3)
        } else {
            showTxt(txtIndex = 3)
        }
})

navImg[3].addEventListener('click', function(){
    showItem(itemIndex = 4)
    showbigImg(imgIndex = 4)
    if(txtIndex > 4){
        showTxtPrev(txtIndex = 4)
        } else {
            showTxt(txtIndex = 4)
        }
})
