let servBtn = document.querySelectorAll('.services-li-item')
let triangle = document.querySelectorAll('.triangle')
let servInfo = document.querySelectorAll('.services-info-wrapp')

function triangleBlock(){
    let servBtnArr = Array.from(servBtn)
    let triangleArr = Array.from(triangle)
    let servInfoArr = Array.from(servInfo)
        for (let i=0; i<servBtnArr.length; i++){
            servBtnArr[i].addEventListener('click', (event) => {
                for(let i of servBtnArr){i.classList.remove("seritemfocus")}
                event.target.classList.add("seritemfocus")
                for(let e of triangleArr){e.style.display = "none"}
                triangleArr[i].style.display = "block";
                
                let filterClass = event.target.dataset['s'];
                servInfoArr.forEach(function(e,i,arr){
                e.classList.remove('inactive-info')
            })
                servInfoArr.forEach(elem => {
                if(!elem.classList.contains(filterClass)){
                    elem.classList.add('inactive-info')
                }
            })
        })
    }  
}

triangleBlock()