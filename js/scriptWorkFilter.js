let workBtn = document.querySelectorAll('.work-li-item')
let filterImg = document.querySelectorAll('.work-img-cont')

function filterImgFun(){
    let filterImg = document.querySelectorAll('.work-img-cont')
    let workBtnArr = Array.from(workBtn);
    let filterImgArr = Array.from(filterImg);

    workBtnArr.forEach(function(e,i,arr){
        e.addEventListener('click', (event) =>{
            let filterClass = event.target.dataset['f'];
            filterImgArr.forEach(function(e,i,arr){
                e.classList.remove('hide-work-img')
            })
            filterImgArr.forEach(elem => {
                if(!elem.classList.contains(filterClass) && filterClass !== "all"){
                    elem.classList.add('hide-work-img')
                }
            })
        })
    })
}

filterImgFun()

let activBlockBtn = document.querySelector(".work-btn")
let twoWorkCont = document.querySelector(".two-work-cont")


function activeImg(){

let arrImg = [
    ["./img/main/work-section/WebDesign/Web_4.png", "web"],
    ["./img/main/work-section/Wordpress/Word_4.png", "word"],
    ["./img/main/work-section/GraphicDesign/Graph_4.png", "graph"],
    ["./img/main/work-section/WebDesign/Web_5.png", "web"],
    ["./img/main/work-section/Wordpress/Word_5.png", "word"],
    ["./img/main/work-section/LandingPages/Land_4.png", "land"],
    ["./img/main/work-section/LandingPages/Land_5.png", "land"],
    ["./img/main/work-section/GraphicDesign/Graph_5.png", "graph"],
    ["./img/main/work-section/Wordpress/Word_6.png", "word"],
    ["./img/main/work-section/GraphicDesign/Graph_6.png", "graph"],
    ["./img/main/work-section/LandingPages/Land_6.png", "land"],
    ["./img/main/work-section/WebDesign/Web_6.png", "web"],
]

let content_el = document.querySelector(".work-section-img-contenir")

arrImg.forEach((attrs) => {   

    let divOne = document.createElement("div");
    let divTwo = document.createElement("div");
    let img = document.createElement("img");
    let spanOne = document.createElement("span");
    let spanTwo = document.createElement("span");

    divOne.classList.add("work-img-cont");
    divTwo.classList.add("hover-block");

    img.src = attrs[0];
    spanOne.innerText = "creative design";
    spanTwo.innerText = "Web Design";

    if(attrs[1] == 'web'){
        divOne.classList.add("web");
    } else if(attrs[1] == 'word'){
        divOne.classList.add("word");
    } else if(attrs[1] == 'graph'){
        divOne.classList.add("graph");
    } else if(attrs[1] == 'land'){
        divOne.classList.add("land");
    }

    divOne.appendChild(divTwo);
    divOne.appendChild(img);

    divTwo.appendChild(spanOne);
    divTwo.appendChild(spanTwo);

    content_el.appendChild(divOne)  
    });

    let content_elArr = content_el.children
    if (content_elArr.length == '36'){
        activBlockBtn.classList.add('work-btn-inactiv')
    }

    filterImgFun()
}

function activeTwoBlock(){
    activBlockBtn.addEventListener('click', activeImg)}
activeTwoBlock()
